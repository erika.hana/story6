from django.test import TestCase, override_settings, Client
from django.urls import resolve
from django.http.request import HttpRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from .apps import Story6UnittestingConfig
from .views import events, deleteEvent
from .models import Events, EventGuests

# tes tes 1 2
# Create your tests here.


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class EventTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.event = Events.objects.create(
            nama_event='halo', deskripsi='halohalohalo')
        self.guest = EventGuests.objects.create(
            event_host=self.event, guest='dodo')

    def test_models_str(self):
        self.assertEqual(str(self.event), self.event.nama_event)
        self.assertEqual(str(self.guest), self.guest.guest)

    def test_event_page_using_view_func(self):
        found = resolve('/event/')
        self.assertEqual(found.func, events)

    def test_event_page_is_exist(self):
        response = self.client.get('/event/')
        self.assertEqual(response.status_code, 200)

    def test_event_page_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/event/', {
                'nama_event': 'halo',
                'deskripsi': 'halohalohalo'
            }
        )
        self.assertEqual(response_post.status_code, 200)

    def test_event_add_guest_page_post_success_and_render_the_result(self):
        response_post = self.client.post(
            '/event/add/Main', {
                'guest': 'main bola'
            }
        )
        self.assertEqual(response_post.status_code, 404)

    def test_apps(self):
        self.assertEqual(Story6UnittestingConfig.name, 'eventpage')

    def test_event_page_add_event_guests(self):
        new_act = Events.objects.create(
            nama_event="kedufan", deskripsi="mau ke dufan")
        new_act.save()
        response = self.client.post('/event/add/kedufan', {'guest': "siapa"})
        self.assertEqual(response.status_code, 302)
