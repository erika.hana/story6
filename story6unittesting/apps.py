from django.apps import AppConfig


class Story6UnittestingConfig(AppConfig):
    name = 'story6unittesting'
